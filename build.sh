#!/bin/sh

# Default image name
[ -f .image.name ] || echo docker-from-docker > .image.name

# Discover image name
IMAGE_NAME=$(cat .image.name)


# This script also contains the Dockerfile content and does not need a build context

# Need the docker group inside container to have same gid as host machine
DOCKERGID=$(stat -c '%g' /var/run/docker.sock)


docker image build  --build-arg DOCKERGID="$DOCKERGID" \
                    --build-arg UID=$(id -u) \
                    --build-arg GID=$(id -g) \
                    --tag "$IMAGE_NAME" "$@" \
                    - <<'EOF'
FROM debian:11-slim

ARG DOCKERGID

ARG UID=1000
ARG GID=1000
ARG USER=dev

# Set up docker group with correct GID and a user (with the correct uid and gid) that is also member of the docker group
RUN groupadd --gid ${DOCKERGID} docker \
    && groupadd --gid ${GID} ${USER} \
    && useradd --uid ${UID} -g ${USER} --groups docker --create-home ${USER}

# Install latest Docker CLI binary
COPY --from=docker:latest /usr/local/bin/docker /usr/local/bin/

# Install latest Docker compose V2 binary
RUN apt-get update && apt-get install -y curl jq && apt-get clean && rm -rf /var/lib/apt/lists/* \
    && [ -d /usr/local/lib/docker/cli-plugins ] || mkdir -p /usr/local/lib/docker/cli-plugins \
    && curl -SL https://github.com/docker/compose/releases/download/$(curl -s https://api.github.com/repos/docker/compose/releases/latest | jq -r .tag_name)/docker-compose-linux-x86_64 \
        -o /usr/local/lib/docker/cli-plugins/docker-compose \
    && chmod +x /usr/local/lib/docker/cli-plugins/docker-compose

USER ${USER}

RUN mkdir /home/${USER}/dev

WORKDIR /home/${USER}/dev

EOF
